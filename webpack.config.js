const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'custom.js',
    path: path.resolve(__dirname, 'assets'),
  },
  module: {
  	rules: [
  		{
  			test: /\.scss$/,
  			exclude: /node_modules/,
  			use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
  		}
  	]
  },
  plugins: [
  	new MiniCssExtractPlugin({
  		filename: "style.css"
  	})
  ]
};